<?php
session_start();
include "../conexion.php";
if (!empty($_POST)) {
    $idpedido = $_POST['idpedido'];
    $estatus = 0;
    $result = 0;

    if (is_numeric($idpedido)) {
        $query = mysqli_query($conn, "SELECT * FROM pedidos WHERE idpedido = '$idpedido' ");
        $result = mysqli_fetch_array($query);
    }

    if ($result != 0) {
        $query_eliminar = mysqli_query($conn, "UPDATE pedidos SET estatus = '$estatus' WHERE idpedido = '$idpedido'");
    }
    if ($query_eliminar) {
        $alert = '<p class=msg_save>Pedido eliminado correctamente.</p>';
    } else {
        $alert = '<p class=msg_error>Error al eliminar el pedido.</p>';
    }
}
//Mostrar Datos    
if (empty($_REQUEST['id'])) {
    header('Location: lista_pedido.php');
}

$id_pedido = $_REQUEST['id'];
$sql = mysqli_query($conn, "SELECT * FROM pedidos WHERE idpedido = $id_pedido and estatus = 1");
$result_sql = mysqli_num_rows($sql);

if ($result_sql == 0) {
    header('Location: lista_pedido.php');
} else {
    $option = '';
    while ($data = mysqli_fetch_array($sql)) {
        $idpedido = $data['idpedido'];
        $nombres = $data['nombres'] . " " .  $data['apellidos'];
        $apellidos = $data['apellidos'];
        $documento = $data['documento'];
        $fecha_nac = $data['fecha_nacimiento'];
        $fecha_nac = date("d/m/Y", strtotime($fecha_nac));
        $correo = $data['email'];
        $descripcion = $data['descripcion'];
        $direccion = $data['direccion'];
        $fecha = $data['fecha'];
        $fecha = date("d/m/Y", strtotime($fecha));
        $hora = $data['hora'];
        $hora = date("g:i a", strtotime("$hora"));
        $total_pedido = number_format($data['total_pedido'], 2, ",", ".");
        $total = number_format($data['monto_total'], 2, ",", ".");
        $abono = number_format($data['abono'], 2, ",", ".");
        $saldo = number_format($data['saldo'], 2, ",", ".");
        $pago = $data['forma_pago'];
        $domicilio = number_format($data['domicilio'], 2, ",", ".");
        $domiciliario = $data['domiciliario'];
    }
}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <?php include "includes/scripts.php" ?>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/functions.js"></script>
    <title>Eliminar Pedido</title>
</head>

<body>
    <?php include "includes/header.php" ?>
    <section id="container">
        <div class="form_register2">
            <h1><i class="fas fa-gifts"></i> Eliminar Pedido</h1>
            <hr>
            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" name="idpedido" id="idpedido" value="<?php echo $idpedido; ?>">
                <table>
                    <td>
                        <label for="nombre">Nombres y Apellidos </label>
                        <input type="text" name="nombres" id="nombres" placeholder="Nombre del Cliente" value="<?php echo $nombres; ?>" disabled>
                        <label for="correo">Dirección de Correo Electrónico </label>
                        <input type="text" name="correo" id="correo" placeholder="Dirección de Correo Electrónico" value="<?php echo $correo; ?>" disabled>
                    </td>
                    <td>
                        <label for="documento">Nro de Documento </label>
                        <input type="text" name="documento" id="documento" placeholder="Número de Documento" value="<?php echo $documento; ?>" disabled>
                        <label for="fecha_nac">Fecha de Nacimiento </label>
                        <input type="text" name="fecha_nac" id="fecha_nac" placeholder="Fecha de Nacimiento" value="<?php echo $fecha_nac; ?>" disabled>
                    </td>
                </table>


                <label for="descripcion">Descripción del Pedido</label>
                <textarea type="textarea" rows="5" name="descripcion" id="descripcion" placeholder="Descripción del Pedido" disabled><?php echo $descripcion; ?></textarea>

                <table>
                    <td>
                        <label for="fecha">Fecha de Entrega </label>
                        <input type="text" name="fecha" id="fecha" placeholder="Fecha de Entrega" value="<?php echo $fecha; ?>" disabled>
                        <label for="hora">Hora de Entrega </label>
                        <input type="text" name="hora" id="hora" placeholder="Hora de Entrega" value="<?php echo $hora; ?>" disabled>
                        <label for="direccion">Dirección de Entrega </label>
                        <textarea type="textarea" rows="2" name="direccion" id="direccion" placeholder="Direccion de Entrega" disabled><?php echo $direccion; ?></textarea>
                        <label for="domiciliario">Datos del Domiciliario </label>
                        <input type="text" name="domiciliario" id="domiciliario" value="<?php echo $domiciliario; ?>" placeholder="Datos del Domiciliario" disabled>
                        <label for="forma">Forma de Pago </label>
                        <input type="text" name="forma" id="forma" placeholder="Forma de Pago" value="<?php echo $pago; ?>" disabled>
                    </td>
                    <td>
                        <label for="total">Total Pedido </label>
                        <input required type="text" name="total_pedido" id="total_pedido" placeholder="Total Pedido" value="<?php echo $total_pedido; ?>" disabled>
                        <label for="forma">Monto Domicilio </label>
                        <input type="text" name="domicilio" id="domicilio" placeholder="Monto Domicilio" value="<?php echo $domicilio; ?>" disabled>
                        <label for="total">Total a Pagar </label>
                        <input type="text" name="total" id="total" placeholder="Total a Pagar" value="<?php echo $total; ?>" disabled>
                        <label for="abono">Monto Abonado </label>
                        <input type="text" name="abono" id="abono" placeholder="Monto abonado" value="<?php echo $abono; ?>" disabled>
                        <label for="saldo">Saldo Pendiente </label>
                        <input type="text" name="saldo" id="saldo" placeholder="Saldo Pendiente" value="<?php echo $saldo; ?>" disabled>

                    </td>
                </table>
                <button type="submit" class="btn_save"><i class="far fa-save"></i> Eliminar Pedido</button>
            </form>
        </div>

    </section>

    <?php include "includes/footer.php" ?>
</body>

</html>