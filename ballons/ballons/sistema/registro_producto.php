<?php
session_start();
if ($_SESSION['rol'] != 1 && $_SESSION['rol'] != 2) {
    header("Location: ./");
}
include "../conexion.php";

if (!empty($_POST)) {
    $alert = '';
    if ($_POST['precio'] <= 0 || $_POST['existencia'] <= 0 || $_POST['minimo'] <= 0 || $_POST['ganancia'] < 0) {
        $alert = '<p class=msg_error>No se pueden registrar cantidades menores o iguales a cero.</p>';
    } else if ($_POST['existencia'] <= $_POST['minimo']) {
        $alert = '<p class=msg_error>El Stock Mínimo no puede ser mayor o igual que la Cantidad.</p>';
    } else {

        $idproducto = $_POST['idproducto'];
        $nombre = $_POST['nombre'];
        $descripcion = $_POST['descripcion'];
        $cantidad = $_POST['existencia'];
        $minimo = $_POST['minimo'];
        $ganancia = $_POST['ganancia'];
        $precio = $_POST['precio'];
        $usuario_id = $_SESSION['idUser'];
        $foto = $_FILES['foto'];
        $nombre_foto = $foto['name'];
        $type = $foto['type'];
        $url_temp = $foto['tmp_name'];
        $imgProducto = 'img_producto.png';

        if ($nombre_foto != '') {
            $destino = 'img/uploads/';
            $img_nombre = 'img_' . md5(date('d-m-Y H:m:s'));
            $imgProducto = $img_nombre . '.jpg';
            $src = $destino . $imgProducto;
        }


        $result = 0;
        if (is_numeric($idproducto)) {
            $query = mysqli_query($conn, "SELECT * FROM productos WHERE id_producto = '$idproducto' ");
            $result = mysqli_fetch_array($query);
        }
        if ($result > 0) {
            $alert = '<p class=msg_error>El producto ya existe.</p>';
        } else {

            $query_insert = mysqli_query($conn, "INSERT INTO productos(id_producto,nombre_producto,descripcion,precio,existencia,minimo,margen_ganancia,imagen,usuario_id) 
                                                    VALUES ('$idproducto','$nombre','$descripcion','$precio','$cantidad','$minimo','$ganancia','$imgProducto','$usuario_id')");

            if ($query_insert) {
                if ($nombre_foto != '') {
                    move_uploaded_file($url_temp, $src);
                }
                $alert = '<p class=msg_save>Producto creado correctamente.</p>';
            } else {
                $alert = '<p class=msg_error>Error al crear el producto.</p>';
            }
        }
    }
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <?php include "includes/scripts.php" ?>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/functions.js"></script>
    <title>Registro Artículo/Servicio</title>
</head>

<body>
    <?php include "includes/header.php" ?>
    <section id="container">
        <div class="form_register">
            <h1><i class="fas fa-gifts"></i> Registro Artículo/Servicio</h1>
            <hr>
            <div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

            <form action="" method="post" enctype="multipart/form-data">
                <label for="idproducto">Código del Producto *</label>
                <input type="text" name="idproducto" id="idproducto" placeholder="Código del Producto" required>
                <label for="nombre">Nombre *</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre del Producto" required>
                <label for="descripcion">Descripción del Producto</label>
                <textarea type="textarea" rows="4" name="descripcion" id="descripcion" placeholder="Descripción del Producto"></textarea>
                <label for="precio">Precio *</label>
                <input type="number" name="precio" id="precio" placeholder="Precio" required>
                <label for="existencia">Cantidad del Producto</label>
                <input type="number" name="existencia" id="existencia" placeholder="Cantidad del Producto">
                <label for="minimo">Stock Mínimo</label>
                <input type="number" name="minimo" id="minimo" placeholder="Stock Mínimo">
                <label for="ganancia">Margen de Ganancia</label>
                <input type="text" name="ganancia" id="ganancia" placeholder="Margen de Ganancia" required>
                <div class="photo">
                    <label for="foto">Foto</label>
                    <div class="prevPhoto">
                        <span class="delPhoto notBlock">X</span>
                        <label for="foto"></label>
                    </div>
                    <div class="upimg">
                        <input type="file" name="foto" id="foto">
                    </div>
                    <div id="form_alert"></div>
                </div>
                <button type="submit" class="btn_save"><i class="far fa-save"></i> Guardar Artículo/Servicio</button>
            </form>
        </div>

    </section>

    <?php include "includes/footer.php" ?>
</body>

</html>