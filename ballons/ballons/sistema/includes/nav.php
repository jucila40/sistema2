<nav>
	<ul>
		<li><a href="#"><i class="fas fa-home"></i> Inicio</a></li>
		<li class="principal">
			<?php
			if ($_SESSION['rol'] == 1) {
			?>
				<a href="#"><i class="fas fa-users"></i> Usuarios</a>
				<ul>
					<li><a href="registro_usuario.php"><i class="fas fa-user-plus"></i> Nuevo Usuario</a></li>
					<li><a href="lista_usuario.php"><i class="fas fa-users"></i> Lista de Usuarios</a></li>
				</ul>
		</li>
	<?php
			}
	?>
	<!-- <li class="principal">
		<a href="#"><i class="far fa-address-card"></i> Clientes</a>
		<ul>
			<li><a href="registro_cliente.php"><i class="fas fa-user-plus"></i> Nuevo Cliente</a></li>
			<li><a href="lista_cliente.php"><i class="fas fa-users"></i> Lista de Clientes</a></li>
		</ul>
	</li> -->
	<?php
	if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 2) {
	?>
		<li class="principal">

			<a href="#"><i class="fas fa-user-friends"></i> Proveedores</a>
			<ul>
				<li><a href="registro_proveedor.php"><i class="fas fa-user-plus"></i> Nuevo Proveedor</a></li>
				<li><a href="lista_proveedor.php"><i class="fas fa-users"></i> Lista de Proveedores</a></li>
			</ul>
		</li>
	<?php
	}
	?>
	<!-- <li class="principal">
					<a href="#"><i class="fas fa-gifts"></i>  Productos</a>
					<ul>
					<?php
					if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 2) {
					?>
						<li><a href="registro_producto.php"><i class="fas fa-gift"></i>  Nuevo Producto</a></li>
					<?php
					}
					?>
						<li><a href="lista_producto.php"><i class="fas fa-gifts"></i>  Lista de Productos</a></li>
					</ul>
				</li>
				<li class="principal">
					<a href="#"><i class="fas fa-file-invoice-dollar"></i>  Facturas</a>
					<ul>
						<li><a href="#"><i class="fas fa-file-invoice"></i> Nuevo Factura</a></li>
						<li><a href="#"><i class="fas fa-inbox"></i>  Facturas</a></li>
					</ul>
				</li> -->

	<li class="principal">
		<a href="#"><i class="fas fa-file-invoice-dollar"></i> Pedidos</a>
		<ul>
			<li><a href="registro_pedido.php"><i class="fas fa-file-invoice"></i> Nuevo Pedido</a></li>
			<li><a href="lista_pedido.php"><i class="fas fa-inbox"></i> Pedidos Por Entregar</a></li>
			<li><a href="lista_pedido2.php"><i class="fas fa-inbox"></i> Pedidos Entregados</a></li>
		</ul>
	</li>
	<li class="principal">
		<a href="respaldo.php"><i class="fas fa-file-invoice-dollar"></i> Respaldar BD</a>

	</li>
	</ul>
</nav>