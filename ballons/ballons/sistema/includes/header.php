<?php

if (empty($_SESSION['active'])) {
    header('location: ../');
}
?>
<header>
    <div class="header">

        <h1>Sistema Administrativo</h1>
        <div class="optionsBar">
            <p>Barranquilla, <?php echo fechaC(); ?></p>
            <span>|</span>
            <span class="user"><?php
                                echo ($_SESSION['rol']);
                                if ($_SESSION['rol'] == 1) {
                                    echo $_SESSION['user'] . ' -' . 'Administrador';
                                } else if ($_SESSION['rol'] == 2) {
                                    echo $_SESSION['user'] . ' -' . 'Supervisor';
                                } else if ($_SESSION['rol'] == 3) {
                                    echo $_SESSION['user'] . ' -' . 'Vendedor';
                                } ?></span>
            <img class="photouser" src="img/user.png" alt="Usuario">
            <a href="salir.php"><img class="close" src="img/salir.png" alt="Salir del sistema" title="Salir"></a>
        </div>
    </div>
    <?php include "nav.php"; ?>
</header>
<div class="modal">
    <div class="bodyModal">
        <form action="" method="post" name="form_ver_product" id="form_ver_product" onsubmit="event.preventDefault();">
            <h1>
                <li class="fa fa-cubes" style="font-size: 35pt;"></li><br> Visualizar Artículo/Servicio
            </h1>
            <h2 class="nameProducto"></h2>
            <label for="codigo">Código del Producto</label>
            <input type="text" name="codigo" id="codigo" disabled>
            <label for="descricpion">Descripción del Producto</label>
            <textarea type="textarea" rows="3" name="descricpion" id="descripcion" disabled></textarea>
            <label for="precio">Precio del Producto</label>
            <input type="number" name="precio" id="precio" disabled>
            <label for="existencia">Existencia del Producto</label>
            <input type="number" name="existencia" id="existencia" disabled>
            <label for="minimo">Stock Mínimo del Producto</label>
            <input type="number" name="minimo" id="minimo" disabled>
            <label for="ganancia">Ganancia del Producto</label>
            <input type="number" name="ganancia" id="ganancia" disabled>
            <a href="#" class="btn_ok closeModal" onclick="closeModal();"><i class="fas fa-times-circle"></i> Cerrar</a>
        </form>
    </div>
</div>


<!-- <div class="modal1">
    <div class="bodyModal">
        <form action="" method="post" name="form_ver_product" id="form_ver_product" onsubmit="event.preventDefault();">
            <h1>
                <li class="fa fa-cubes" style="font-size: 35pt;"></li><br> Eliminar Artículo/Servicio
            </h1>
            <p>¿Está seguro de eliminar el siguiente Artículo/Servicio?</p>
            <h2 name="nombre" class="nameProducto"></h2>

            <a href="#" class="btn_ok closeModal" onclick="closeModal1();"><i class="fas fa-times-circle"></i> Cerrar</a>
        </form>
    </div>
</div> -->