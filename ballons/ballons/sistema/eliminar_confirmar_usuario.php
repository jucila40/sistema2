<?php
session_start();
if($_SESSION['rol'] != 1)
{
    header("Location: ./");
}
    include "../conexion.php";

    if(!empty($_POST))
    {
        if($_POST['idusuario']==2){
            header("Location: lista_usuario.php");
            mysqli_close($conn);
            exit;
        }
        $idusuario = $_POST['idusuario'];

        //$query_delete = mysqli_query($conn,"DELETE FROM usuario WHERE idusuario = $idusuario");
        $query_delete = mysqli_query($conn,"UPDATE usuario SET status = 0 WHERE idusuario = $idusuario");
        mysqli_close($conn);
        if($query_delete)
        {
            header("Location: lista_usuario.php");
            mysqli_close($conn);
        }else{
            echo "Error al eliminar";
        }

    }

    if(empty($_REQUEST['id'])||$_REQUEST['id']==2)
    {
        header("Location: lista_usuario.php");
    }else{
      

        $idusuario = $_REQUEST['id'];

        $query = mysqli_query($conn,"SELECT u.nombre,u.usuario,r.rol 
                                        FROM usuario u
                                        INNER JOIN
                                        rol r
                                        ON u.rol = r.idrol
                                        WHERE u.idusuario = $idusuario");
        mysqli_close($conn);
        $result = mysqli_num_rows($query);

        if($result > 0)
        {
            while ($data = mysqli_fetch_array($query)){
                $nombre = $data['nombre'];
                $usuario = $data['usuario'];
                $rol = $data['rol'];
            }
        }else{
            header("Location: lista_usuario.php");
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php" ?>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Eliminar Usuario</title>
</head>
<body>
	<?php include "includes/header.php" ?>
	<section id="container">
		<div class="data_delete">
            <i class="fas fa-user-times fa-7x" style = "color: #cc2a2a"></i>
            <br><br><br>
            <h2>¿Está seguro de eliminar el siguiente registro?</h2>
            <br>
            <p>Nombre: <span><?php echo $nombre; ?></span> </p>
            <p>Usuario: <span><?php echo $usuario; ?></span> </p>
            <p>Tipo Usuario: <span><?php echo $rol; ?></span> </p>
            <br>
            <form action="" method = "post">
                <input type="hidden" name="idusuario" value="<?php echo $idusuario; ?>">
                <a href="lista_usuario.php" class="btn_cancel"><i class="fas fa-ban"></i>  Cancelar</a>
                <button type="submit" class="btn_ok"><i class="fas fa-trash"></i>  Aceptar</button>
            </form>
        </div>
	</section>

	<?php include "includes/footer.php" ?>
</body>
</html>