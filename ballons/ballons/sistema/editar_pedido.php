<?php
session_start();
// if ($_SESSION['rol'] != 1 && $_SESSION['rol'] != 2) {
//     header("Location: ./");
// }
include "../conexion.php";
$saldo = 0;
if (!empty($_POST)) {
    $idpedido = $_POST['idpedido'];
    $nombres = $_POST['nombres'];
    $apellidos = $_POST['apellidos'];
    $documento = $_POST['documento'];
    $correo = $_POST['correo'];
    $fecha_nac = $_POST['fecha_nac'];
    $descripcion = $_POST['descripcion'];
    $direccion = $_POST['direccion'];
    $fecha = $_POST['fecha'];
    $hora = $_POST['hora'];
    $total_pedido = $_POST['total_pedido'];
    $domicilio = $_POST['domicilio'];
    $total = $_POST['total_pagar'];
    $abono = $_POST['abono'];
    $saldo = $_POST['saldo'];
    $pago = $_POST['forma'];
    $domiciliario = $_POST['domiciliario'];
    $estatus = 1;
    $result = 0;

    if (is_numeric($idpedido)) {
        $query = mysqli_query($conn, "SELECT * FROM pedidos WHERE idpedido = '$idpedido' ");
        $result = mysqli_fetch_array($query);
    }

    if ($result != 0) {

        $query_update = mysqli_query($conn, "UPDATE pedidos SET nombres = '$nombres', apellidos = '$apellidos', documento = '$documento',
                                                                    fecha_nacimiento = '$fecha_nac', email = '$correo', fecha = '$fecha', hora = '$hora',
                                                                    direccion = '$direccion', descripcion = '$descripcion', total_pedido = '$total_pedido',
                                                                    domicilio = '$domicilio', monto_total = '$total', abono = '$abono', saldo = '$saldo',
                                                                    forma_pago = '$pago', domiciliario = '$domiciliario', estatus =  '$estatus' WHERE idpedido = '$idpedido'");
    }
    if ($query_update) {
        $alert = '<p class=msg_save>Pedido actualizado correctamente.</p>';
    } else {
        $alert = '<p class=msg_error>Error al actualizar el pedido.</p>';
    }
}


//Mostrar Datos    
if (empty($_REQUEST['id'])) {
    header('Location: lista_pedido.php');
}

$id_pedido = $_REQUEST['id'];
$sql = mysqli_query($conn, "SELECT * FROM pedidos WHERE idpedido = $id_pedido and estatus = 1");
$result_sql = mysqli_num_rows($sql);

if ($result_sql == 0) {
    header('Location: lista_pedido.php');
} else {
    $option = '';
    while ($data = mysqli_fetch_array($sql)) {
        $idpedido = $data['idpedido'];
        $nombres = $data['nombres'];
        $apellidos = $data['apellidos'];
        $documento = $data['documento'];
        $fecha_nac = $data['fecha_nacimiento'];
        $correo = $data['email'];
        $descripcion = $data['descripcion'];
        $direccion = $data['direccion'];
        $fecha = $data['fecha'];
        $hora = $data['hora'];
        $hora = date("g:i a", strtotime("$hora"));
        $total_pedido = number_format($data['total_pedido'], 2, ",", ".");
        $total = number_format($data['monto_total'], 2, ",", ".");
        $abono = number_format($data['abono'], 2, ",", ".");
        $saldo = number_format($data['saldo'], 2, ",", ".");
        $pago = $data['forma_pago'];
        $domiciliario = $data['domiciliario'];
        $domicilio = number_format($data['domicilio'], 2, ",", ".");
    }
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <?php include "includes/scripts.php" ?>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/functions.js"></script>
    <title>Editar Pedido</title>
</head>

<body>
    <?php include "includes/header.php" ?>
    <section id="container">
        <div class="form_register2">
            <h1><i class="fas fa-gifts"></i> Editar Pedido</h1>
            <hr>
            <div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

            <form action="" method="post" enctype="multipart/form-data" id="editar">
                <input type="hidden" name="idpedido" id="idpedido" value="<?php echo $idpedido; ?>">
                <table>
                    <td>
                        <label for="nombre">Nombres del Cliente </label>
                        <input type="text" name="nombres" id="nombres" placeholder="Nombres del Cliente" value="<?php echo $nombres; ?>" required>
                        <label for="documento">Nro de Documento </label>
                        <input type="text" name="documento" id="documento" placeholder="Número de Documento" value="<?php echo $documento; ?>" required>
                    </td>
                    <td>
                        <label for="apellido">Apellidos del Cliente </label>
                        <input type="text" name="apellidos" id="apellidos" placeholder="Apellidos del Cliente" value="<?php echo $apellidos; ?>" required>
                        <label for="correo">Dirección de Correo Electrónico </label>
                        <input type="text" name="correo" id="correo" placeholder="Dirección de Correo Electrónico" value="<?php echo $correo; ?>">
                    </td>
                </table>

                <table style="width:43.5%;">
                    <td>
                        <label for="fecha">Fecha de Nacimiento </label>
                        <input type="text" name="fecha_nac" id="fecha_nac" placeholder="Fecha de Nacimiento" value="<?php echo $fecha_nac; ?>">
                    </td>
                </table>

                <label for="descripcion">Descripción del Pedido</label>
                <textarea type="textarea" rows="5" name="descripcion" id="descripcion" placeholder="Descripción del Pedido" required><?php echo $descripcion; ?></textarea>

                <table>
                    <td>
                        <label for="fecha">Fecha de Entrega </label>
                        <input type="text" name="fecha" id="fecha" placeholder="Fecha de Entrega" value="<?php echo $fecha; ?>" required>
                        <label for="hora">Hora de Entrega </label>
                        <input type="text" name="hora" id="hora" placeholder="Hora de Entrega" value="<?php echo $hora; ?>" required>
                        <label for="direccion">Dirección de Entrega </label>
                        <textarea type="textarea" rows="2" name="direccion" id="direccion" placeholder="Direccion de Entrega" required><?php echo $direccion; ?></textarea>
                        <label for="domiciliario">Datos del Domiciliario </label>
                        <input type="text" name="domiciliario" id="domiciliario" value="<?php echo $domiciliario; ?>" placeholder="Datos del Domiciliario">
                        <label for="forma">Forma de Pago </label>
                        <select name="forma">
                            <option value="Efectivo">Efectivo</option>
                            <option value="Transferencia" selected>Transferencia</option>
                        </select>
                    </td>
                    <td>
                        <label for="total">Total Pedido </label>
                        <input required type="text" name="total_pedido" id="total_pedido" placeholder="Total Pedido" value="<?php echo $total_pedido; ?>" onchange="sumar(this.value);" required>
                        <label for="forma">Monto Domicilio </label>
                        <input type="text" name="domicilio" id="domicilio" placeholder="Monto Domicilio" value="<?php echo $domicilio; ?>" onchange="sumar(this.value);" required>
                        <label for="total">Total a Pagar </label>
                        <input type="text" name="total_pagar" id="total_pagar" placeholder="Total a Pagar" value="<?php echo $total; ?>">
                        <label for="abono">Monto Abonado </label>
                        <input type="text" name="abono" id="abono" placeholder="Monto abonado" value="<?php echo $abono; ?>" onchange="abonos(this.value);" required>
                        <label for="saldo">Saldo Pendiente </label>
                        <input type="text" name="saldo" id="saldo" placeholder="Saldo Pendiente" value="<?php echo $saldo; ?>" onchange="abonos(this.value);">

                    </td>
                </table>
                <button type="submit" class="btn_save"><i class="far fa-save"></i> Actualizar Pedido</button>
            </form>
        </div>

    </section>

    <?php include "includes/footer.php" ?>
</body>

</html>