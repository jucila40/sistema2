<?php
session_start();

    include "../conexion.php";

    if(!empty($_POST))
    {
        $alert='';
        $iduser = $_REQUEST['id'];
        $tipo_documento = $_POST['tipo_documento'];
        $documento = $_POST['idcliente'];
        $nombre = $_POST['nombre'];
        $direccion = $_POST['direccion'];
        $telefono = $_POST['telefono'];
        $email= $_POST['correo'];
        $fechanac = $_POST['fechanac'];
        $instagram = $_POST['instagram'];
        $usuario_id = $_SESSION['idUser'];

            
        $result = 0;
        if(is_numeric($documento))
        {
            $query = mysqli_query($conn, "SELECT * FROM clientes WHERE id_cliente = '$documento' ");
            $result = mysqli_fetch_array($query);
        }
            //$result = count($result);
            if($result != 0 &&  $iduser != $documento )
            {
                $alert='<p class=msg_error>El Número de documento ya existe.</p>';
            }else{
                $sql_update = mysqli_query($conn, "UPDATE clientes SET id_cliente = $documento, nombre_cliente = '$nombre', direccion_cliente= '$direccion',
                                                         telefono_cliente = '$telefono', email_cliente = '$email', 
                                                         fecha_nacimiento = '$fechanac', tipo_documento = '$tipo_documento', instagram_cliente = '$instagram' WHERE id_cliente = '$iduser'");
                 
                if($sql_update){
                    $alert='<p class=msg_save>Cliente actualizado correctamente.</p>';
                }else{
                    $alert='<p class=msg_error>Error al actualizar el cliente.</p>';
                }
            }
        
      

    }

    //Mostrar Datos    
    if(empty($_REQUEST['id']))
    {
        header('Location: lista_cliente.php');
        mysqli_close($conn);
    }

    $iduser = $_REQUEST['id'];
    $sql = mysqli_query($conn, "SELECT * FROM clientes WHERE id_cliente = $iduser and estatus = 1");
    $result_sql = mysqli_num_rows($sql);

    if($result_sql == 0 && $sql_update== 0)
    {
        header('Location: lista_cliente.php');
    }else{
        $option = '';
        while($data = mysqli_fetch_array($sql)){
            
            $tipo_documento = $data['tipo_documento'];
            $documento = $data['id_cliente'];
            $nombre = $data['nombre_cliente'];
            $direccion = $data['direccion_cliente'];
            $telefono = $data['telefono_cliente'];
            $email= $data['email_cliente'];
            $fechanac = $data['fecha_nacimiento'];
            $instagram = $data['instagram_cliente'];
  
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php" ?>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Actualizar Cliente</title>
</head>
<body>
	<?php include "includes/header.php" ?>
	<section id="container">
        <div class="form_register">
            <h1><i class="fas fa-user-edit"></i> Actualizar Cliente</h1>
            <hr>
            <div class="alert"><?php echo isset($alert) ? $alert : '';?></div>
            
            <form action="" method="post">
                <label for="tipo_documento">Tipo Documento</label>
                <?php
                        include "../conexion.php";
                        $query_documento = mysqli_query($conn, "SELECT * FROM clientes");
                        $result_documento = mysqli_num_rows($query_documento);  
                    ?>
                    <select name="tipo_documento" id="tipo_documento" class="notItemOne">
                        <?php
                        echo $option;
                            if($result_documento > 0){
                            $data = mysqli_fetch_array($query_documento);
                        ?>
                                <option value="<?php echo $tipo_documento; ?>"><?php echo $tipo_documento; ?></option>
                                <?php                
                                
                            }
                ?>
                                <option value="Cédula Ciudadanía">Cédula Ciudadanía</option>
                                <option value="Cédula Extranjería">Cédula Extranjería</option>
                                <option value="Pasaporte">Pasaporte</option>
                                <option value="NIT">NIT</option>
                                <option value="Permiso Especial">Permiso Especial</option>
       
                </select>
                
                <label for="idcliente">N° Documento *</label>
                <input type="text" name = "idcliente" value = "<?php if(!$_POST){echo $iduser; }else{echo $documento;}?> "required>
                <label for="nombre">Nombre *</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre completo" value="<?php echo $nombre; ?>" required>
                <label for="direccion">Dirección *</label>
                <input type="text" name="direccion" id="direccion" placeholder="Dirección" value="<?php echo $direccion; ?>" required>
                <label for="telefono">Teléfono *</label>
                <input type="text" name="telefono" id="telefono" placeholder="Teléfono" value="<?php echo $telefono; ?>" required>
                <label for="correo">Correo Electrónico</label>
                <input type="email" name="correo" id="correo" placeholder="Correo electrónico" value="<?php echo $email; ?>">
                <label for="fechanac">Fecha de Nacimiento</label>
                <input type="date" name="fechanac" id="fechanac" value="<?php echo $fechanac; ?>">
                <label for="instagram">Instagram</label>
                <input type="text" name="instagram" id="instagram" placeholder="Instagram" value="<?php echo $instagram; ?>">
                <button type="submit" class="btn_save"><i class="fas fa-edit"></i> Actualizar Cliente</button>
            </form>     
                   
        </div>

	</section>

	<?php include "includes/footer.php" ?>
</body>
</html>