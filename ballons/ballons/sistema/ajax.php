<?php
include "../conexion.php";

$op = isset($_POST["op"]) ? $_POST["op"] : "";

switch ($op) {
    case 1:
        if ($_POST['action'] == 'infoProducto') {
            $id_producto = $_POST['producto'];
            $query = mysqli_query($conn, "SELECT * FROM productos WHERE id_producto = $id_producto AND estatus = 1");

            mysqli_close($conn);
            $result = mysqli_num_rows($query);

            if ($result > 0) {
                $data = mysqli_fetch_assoc($query);
                echo json_encode($data, JSON_UNESCAPED_UNICODE);
                exit;
            }

            echo 'error';
            exit;
        }
        break;

    case 2:
        $fecha = $_POST['fecha'];

        $query = mysqli_query($conn, "SELECT * FROM pedidos WHERE fecha = '$fecha' AND estatus = 1");
        $result = mysqli_num_rows($query);
        if ($result >= 10) {
            die(json_encode(["respuesta" => "OK"]));
        } else {
            die(json_encode(["respuesta" => "FALLO"]));
        }
        break;
}


exit;
