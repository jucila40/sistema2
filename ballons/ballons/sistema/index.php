<?php
session_start();
?>
<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php" ?>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Ballons&Decoration | Sistema de Ventas</title>
</head>

<body>
	<?php include "includes/header.php" ?>
	<section id="container">
		<h1>Bienvenido al Sistema</h1>
	</section>

	<?php include "includes/footer.php"
	?>
</body>

</html>