<?php
session_start();
// if ($_SESSION['rol'] != 1 && $_SESSION['rol'] != 2) {
//     header("Location: ./");
// }
include "../conexion.php";
$saldo = 0;
if (!empty($_POST)) {
    $nombres = $_POST['nombres'];
    $apellidos = $_POST['apellidos'];
    $documento = $_POST['documento'];
    $correo = $_POST['correo'];
    $fecha_nac = $_POST['fecha_nac'];
    $descripcion = $_POST['descripcion'];
    $direccion = $_POST['direccion'];
    $fecha = $_POST['fecha'];
    $hora = $_POST['hora'];
    $total_pedido = $_POST['total_pedido'];
    $domicilio = $_POST['domicilio'];
    $total = $total_pedido + $domicilio;
    $abono = $_POST['abono'];
    $saldo = $total - $abono;
    $pago = $_POST['forma'];
    $domiciliario = $_POST['domiciliario'];
    $estatus = 1;
    $result = 0;

    $query_insert = mysqli_query($conn, "INSERT INTO pedidos(nombres,apellidos,documento,fecha_nacimiento,email,fecha,hora,direccion,descripcion,total_pedido,domicilio,monto_total,abono,saldo,forma_pago,domiciliario,estatus) 
                                                    VALUES ('$nombres','$apellidos','$documento','$fecha_nac','$correo','$fecha','$hora','$direccion','$descripcion','$total_pedido','$domicilio','$total','$abono','$saldo','$pago','$domiciliario','$estatus')");

    if ($query_insert) {
        $alert = '<p class=msg_save>Pedido registrado correctamente.</p>';
    } else {
        $alert = '<p class=msg_error>Error al registrar el pedido.</p>';
    }
}


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <?php include "includes/scripts.php" ?>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/functions.js"></script>
    <title>Registro Pedido</title>
</head>

<body>
    <?php include "includes/header.php" ?>
    <section id="container">
        <div class="form_register2">
            <h1><i class="fas fa-gifts"></i> Registro Pedido</h1>
            <hr>
            <div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

            <form action="" method="post" enctype="multipart/form-data" id="guardar">
                <table>
                    <td>
                        <label for="nombre">Nombres del Cliente </label>
                        <input type="text" name="nombres" id="nombres" placeholder="Nombres del Cliente" required>
                        <label for="documento">Nro de Documento </label>
                        <input type="text" name="documento" id="documento" placeholder="Número de Documento" required>

                    </td>
                    <td>
                        <label for="apellido">Apellidos del Cliente </label>
                        <input type="text" name="apellidos" id="apellidos" placeholder="Apellidos del Cliente" required>
                        <label for="correo">Dirección de Correo Electrónico </label>
                        <input type="text" name="correo" id="correo" placeholder="Dirección de Correo Electrónico">
                    </td>
                </table>

                <table style="width:43.5%;">
                    <td>
                        <label for="fecha">Fecha de Nacimiento </label>
                        <input type="date" name="fecha_nac" id="fecha_nac" placeholder="Fecha de Nacimiento">
                    </td>
                </table>

                <label for="descripcion">Descripción del Pedido</label>
                <textarea type="textarea" rows="5" name="descripcion" id="descripcion" placeholder="Descripción del Pedido" required></textarea>

                <table>
                    <td>
                        <label for="fecha">Fecha de Entrega </label>
                        <input type="date" onchange="validar_fecha()" name="fecha" id="fecha" placeholder="Fecha de Entrega" required>
                        <label for="hora">Hora de Entrega </label>
                        <input type="time" name="hora" id="hora" placeholder="Hora de Entrega" required>
                        <label for="direccion">Dirección de Entrega </label>
                        <textarea type="textarea" rows="2" name="direccion" id="direccion" placeholder="Direccion de Entrega" required></textarea>
                        <label for="domiciliario">Datos del Domiciliario </label>
                        <input type="text" name="domiciliario" id="domiciliario" placeholder="Datos del Domiciliario">
                        <label for="forma">Forma de Pago </label>
                        <select name="forma">
                            <option value="Efectivo">Efectivo</option>
                            <option value="Transferencia" selected>Transferencia</option>
                        </select>
                    </td>
                    <td>
                        <label for="total">Total Pedido </label>
                        <input required type="number" pattern="[0-9,.]+" name="total_pedido" id="total_pedido" placeholder="Total Pedido" onchange="sumar(this.value);" required>
                        <label for="forma">Monto Domicilio </label>
                        <input type="number" name="domicilio" id="domicilio" placeholder="Monto Domicilio" onchange="sumar(this.value);" required>
                        <label for="total">Total a Pagar </label>
                        <input type="text" name="total_pagar" id="total_pagar" placeholder="Total a Pagar" disabled>
                        <label for="abono">Monto Abonado </label>
                        <input type="number" name="abono" id="abono" placeholder="Monto abonado" onchange="abonos(this.value);" required>
                        <label for="saldo">Saldo Pendiente </label>
                        <input type="text" name="saldo" id="saldo" placeholder="Saldo Pendiente" onchange="abonos(this.value);" disabled>

                    </td>
                </table>
                <button type="submit" class="btn_save"><i class="far fa-save"></i> Guardar Pedido</button>
            </form>
        </div>

    </section>

    <?php include "includes/footer.php" ?>
</body>

</html>