$(document).ready(function(){

    //--------------------- SELECCIONAR FOTO PRODUCTO ---------------------
    $("#foto").on("change",function(){
    	var uploadFoto = document.getElementById("foto").value;
        var foto       = document.getElementById("foto").files;
        var nav = window.URL || window.webkitURL;
        var contactAlert = document.getElementById('form_alert');
        
            if(uploadFoto !='')
            {
                var type = foto[0].type;
                var name = foto[0].name;
                if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png')
                {
                    contactAlert.innerHTML = '<p class="errorArchivo">El archivo no es válido.</p>';                        
                    $("#img").remove();
                    $(".delPhoto").addClass('notBlock');
                    $('#foto').val('');
                    return false;
                }else{  
                        contactAlert.innerHTML='';
                        $("#img").remove();
                        $(".delPhoto").removeClass('notBlock');
                        var objeto_url = nav.createObjectURL(this.files[0]);
                        $(".prevPhoto").append("<img id='img' src="+objeto_url+">");
                        $(".upimg label").remove();
                        
                    }
              }else{
              	alert("No selecciono foto");
                $("#img").remove();
              }              
    });

    $('.delPhoto').click(function(){
    	$('#foto').val('');
    	$(".delPhoto").addClass('notBlock');
    	$("#img").remove();

        if ($("#foto_actual") && $("#foto_remove")){
            $("#foto_remove").val('img_producto.png');
        }
    });
    
    // Modal Form Ver Product

    $('.ver_producto').click(function(e){
        /*Act on the event*/
        e.preventDefault();
        var producto = $(this).attr('producto');
        var action = 'infoProducto';

        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            async: true,
            data: {action:action,producto:producto},
        
            success: function(response){
                if(response != 'error'){
                    var info = JSON.parse(response);
                    $('#codigo').val(info.id_producto);
                    $('#descripcion').val(info.descripcion);
                    $('#precio').val(info.precio);
                    $('#existencia').val(info.existencia);
                    $('#minimo').val(info.minimo);
                    $('#ganancia').val(info.margen_ganancia);
                    $('.nameProducto').html(info.nombre_producto);
                }
            },

            error: function(error){
                console.log(error);
            }
        });

        $('.modal').fadeIn();
    });

    $('.del_producto').click(function(e){
        /*Act on the event*/
        e.preventDefault();
        var producto = $(this).attr('producto');
        var action = 'infoProducto';

        $.ajax({
            url: 'ajax.php',
            type: 'POST',
            async: true,
            data: {action:action,producto:producto},
        
            success: function(response){
                if(response != 'error'){
                    var info = JSON.parse(response);
                    $('#codigo').val(info.id_producto);
                    $('#nombre').val(info.nombre_producto);
                    $('#descripcion').val(info.descripcion);
                    $('#precio').val(info.precio);
                    $('#existencia').val(info.existencia);
                    $('#minimo').val(info.minimo);
                    $('#ganancia').val(info.margen_ganancia);
                    $('.nameProducto').html(info.nombre_producto);
                }
            },

            error: function(error){
                console.log(error);
            }
        });

        $('.modal1').fadeIn();
    });

    
});

function closeModal(){
    $('.modal').fadeOut();
    $('.modal1').fadeOut();
}

/* Sumar los montos del pedido. */
function sumar (valor) {	
    var total = 0;
    valor = parseFloat(valor); // Convertir el valor a un entero (número).
    total_pedido = document.getElementById('total_pedido').value;
    domicilio = (document.getElementById('domicilio').value == null || document.getElementById('domicilio').value == "") ? 0 : document.getElementById('domicilio').value;
	
    //total = document.getElementById('total_pagar').value;
    // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
    total = (total == null || total == undefined || total == "") ? 0 : total;
	
    /* Esta es la suma. */
    total = (parseFloat(total_pedido) + parseFloat(domicilio));
    // Colocar el resultado de la suma en el control "span".
    document.getElementById("total_pagar").value  = total;
}

/* Sumar los montos del pedido. */
function abonos (valor) {	
    var total = 0;
    valor = parseFloat(valor); // Convertir el valor a un entero (número).
    total_pedido = document.getElementById('total_pagar').value;
    abono = (document.getElementById('abono').value == null || document.getElementById('abono').value == "") ? 0 : document.getElementById('abono').value;
	
    //total = document.getElementById('total_pagar').value;
    // Aquí valido si hay un valor previo, si no hay datos, le pongo un cero "0".
    total = (total == null || total == undefined || total == "") ? 0 : total;
	
    /* Esta es la suma. */
    total = (parseFloat(total_pedido) - parseFloat(abono));
    // Colocar el resultado de la suma en el control "span".
    document.getElementById("saldo").value  = total;
}

function validar_fecha(){
    fecha = document.getElementById('fecha').value;
    $.ajax({
        url: "ajax.php",
        type: "POST",
        data: {
          op: 2,
          fecha: fecha,

        },
        dataType: "JSON",
        success: function (r) {
          if (r.respuesta == "OK") {
            var opcion = confirm("Ya excedio el número de pedidos en el día... Desea registrarlo??");
            if (opcion != true){
                $("#fecha").val("");
            }
          }
        },
      });
}

function continuar(){
    console.log("SI ENTRE");
    
    $("#guardar").submit();
}
