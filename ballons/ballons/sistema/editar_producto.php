<?php
    session_start();
    if($_SESSION['rol'] != 1 && $_SESSION['rol'] != 2)
    {
        header("Location: ./");
    }
    include "../conexion.php";

    if(!empty($_POST))
    {
        $alert='';
        if($_POST['precio'] <= 0 || $_POST['existencia'] <= 0 || $_POST['minimo'] <= 0 || $_POST['ganancia'] < 0)
        {
            $alert='<p class=msg_error>No se pueden registrar cantidades menores o iguales a cero.</p>';
        }else if ($_POST['existencia'] <= $_POST['minimo'] ){
            $alert='<p class=msg_error>El Stock Mínimo no puede ser mayor o igual que la Cantidad.</p>';
        }else{
              
            $id_prod = $_REQUEST['id'];
            $idproducto = $_POST['idproducto'];
            $nombre = $_POST['nombre'];
            $descripcion = $_POST['descripcion'];
            $cantidad = $_POST['existencia'];
            $minimo= $_POST['minimo'];
            $ganancia = $_POST['ganancia'];
            $precio = $_POST['precio'];
            $imgProducto = $_POST['foto_actual'];
            $imgRemove=$_POST['foto_remove'];
            $usuario_id = $_SESSION['idUser'];
            $foto = $_FILES['foto'];
            $nombre_foto = $foto['name'];
            $type = $foto['type'];
            $url_temp = $foto['tmp_name'];
            $upd = '';

            if($nombre_foto != '')
            {
                $destino = 'img/uploads/';
                $img_nombre = 'img_'.md5(date('d-m-Y H:m:s'));
                $imgProducto = $img_nombre.'.jpg';
                $src = $destino.$imgProducto;
            }else{
                if($_POST['foto_actual'] != $_POST['foto_remove']){
                    $imgProducto = 'img_producto.png';
                }
            }
            

            $result = 0;
            if(is_numeric($idproducto))
            {
                $query = mysqli_query($conn, "SELECT * FROM productos WHERE id_producto = '$idproducto' ");
                $result = mysqli_fetch_array($query);
            }
            if($result != 0 &&  $id_prod != $idproducto)
            {
                $alert='<p class=msg_error>El código del producto ya existe.</p>';
            }else{

                $query_update = mysqli_query($conn,"UPDATE productos SET id_producto = $idproducto, nombre_producto='$nombre', 
                                                                        descripcion='$descripcion', precio= '$precio', existencia='$cantidad',
                                                                        minimo='$minimo', margen_ganancia='$ganancia', imagen='$imgProducto' 
                                                                        WHERE id_producto = $id_prod"); 
                                                   
                                           
                if($query_update){
                    if(($nombre_foto != '' && ($_POST['foto_actual'] != 'img_producto.png')) || ($_POST['foto_actual'] != $_POST['foto_remove']))
                    {
                        unlink('img/uploads/'.$_POST['foto_actual']);
                    }
                    if($nombre_foto != '')
                    {
                        move_uploaded_file($url_temp,$src);
                    }
                    $alert='<p class=msg_save>Producto actualizado correctamente.</p>';
                }else{
                    $alert='<p class=msg_error>Error al actualizar el producto.</p>';
                }
            }
        }
    }

    //Mostrar Datos    
     if(empty($_REQUEST['id']))
     {
         header('Location: lista_producto.php');
     }
 
     $id_prod = $_REQUEST['id'];
     $sql = mysqli_query($conn, "SELECT * FROM productos WHERE id_producto = $id_prod and estatus = 1");
     $result_sql = mysqli_num_rows($sql);

     $foto = '';
     $classRemove = 'notBlock';
 
     if($result_sql == 0 && $sql_update== 0)
     {
         header('Location: lista_producto.php');
     }else{
         $option = '';
         while($data = mysqli_fetch_array($sql)){
            if($data['imagen'] != 'img_producto.png'){
                $classRemove = '';
                $foto = '<img id="img" src="img/uploads/'.$data['imagen'].'" alt="Producto">';
            }
            $idproducto = $data['id_producto'];
            $nombre = $data['nombre_producto'];
            $descripcion = $data['descripcion'];
            $cantidad = $data['existencia'];
            $minimo= $data['minimo'];
            $ganancia = $data['margen_ganancia'];
            $precio = $data['precio'];
            $imagen = $data['imagen'];
               
         }
     }

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php" ?>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Editar Artículo/Servicio</title>
</head>
<body>
	<?php include "includes/header.php" ?>
	<section id="container">
        <div class="form_register">
            <h1><i class="fas fa-gifts"></i>  Editar Artículo/Servicio</h1>
            <hr>
            <div class="alert"><?php echo isset($alert) ? $alert : '';?></div>

            <form action="" method="post" enctype="multipart/form-data">
                <input type="hidden" id="foto_actual" name="foto_actual" value="<?php echo $imagen; ?>">
                <input type="hidden" id="foto_remove" name="foto_remove" value="<?php echo $imagen; ?>">
                <label for="idproducto">Código del Producto *</label>
                <input type="text" name="idproducto" id="idproducto" placeholder="Código del Producto" value = "<?php if(!$_POST){echo $id_prod; }else{echo $idproducto;}?> " required>
                <label for="nombre">Nombre *</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre del Producto" value="<?php echo $nombre; ?>" required>
                <label for="descripcion">Descripción del Producto</label>
                <textarea type="textarea" rows="3" name="descripcion" id="descripcion" placeholder="Descripción del Producto" ><?php echo $descripcion; ?></textarea>
                <label for="precio">Precio *</label>
                <input type="number" name="precio" id="precio" placeholder="Precio" value="<?php echo $precio; ?>" required>
                <label for="existencia">Cantidad del Producto</label>
                <input type="number" name="existencia" id="existencia" placeholder="Cantidad del Producto" value="<?php echo $cantidad; ?>">
                <label for="minimo">Stock Mínimo</label>
                <input type="number" name="minimo" id="minimo" placeholder="Stock Mínimo" value="<?php echo $minimo; ?>">
                <label for="ganancia">Margen de Ganancia</label>
                <input type="number" name="ganancia" id="ganancia" placeholder = "Margen de Ganancia" value="<?php echo $ganancia; ?>" required>
                <div class="photo">
                    <label for="foto">Foto</label>
                    <div class="prevPhoto">
                    <span class="delPhoto <?php echo $classRemove; ?>">X</span>
                    <label for="foto"></label>
                    <?php echo $foto; ?>
                    </div>
                    <div class="upimg">
                        <input type="file" name="foto" id="foto" value="<?php echo $foto; ?>">
                    </div>
                    <div id="form_alert"></div>
                </div>
                <button type="submit" class="btn_save"><i class="far fa-save"></i> Actualizar Artículo/Servicio</button>
            </form>        
        </div>

	</section>

	<?php include "includes/footer.php" ?>
</body>
</html>