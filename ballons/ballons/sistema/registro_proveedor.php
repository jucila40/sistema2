<?php
    session_start();
    if($_SESSION['rol'] != 1 && $_SESSION['rol'] != 2)
    {
        header("Location: ./");
    }

    include "../conexion.php";

    if(!empty($_POST))
    {
        $alert='';
              
            $tipo_documento = $_POST['tipo_documento'];
            $documento = $_POST['idproveedor'];
            $nombre = $_POST['nombre'];
            $direccion = $_POST['direccion'];
            $telefono = $_POST['telefono'];
            $email= $_POST['correo'];
            $empresa = $_POST['empresa'];
            $instagram = $_POST['instagram'];
            $pagina_web = $_POST['pagina_web'];
            $usuario_id = $_SESSION['idUser'];
            
            $result = 0;
            if(is_numeric($documento))
            {
                $query = mysqli_query($conn, "SELECT * FROM proveedores WHERE id_proveedor = '$documento' ");
                $result = mysqli_fetch_array($query);
            }
            if($result > 0)
            {
                $alert='<p class=msg_error>El proveedor ya existe.</p>';
            }else{

                $query_insert = mysqli_query($conn,"INSERT INTO proveedores(id_proveedor,nombre_proveedor,direccion_proveedor,telefono_proveedor,email_proveedor,tipo_documento,empresa,instagram_proveedor,pagina_web,usuario_id) 
                                                    VALUES ('$documento','$nombre','$direccion','$telefono','$email', '$tipo_documento','$empresa','$instagram', '$pagina_web', '$usuario_id')");
                if($query_insert){
                    $alert='<p class=msg_save>Proveedor creado correctamente.</p>';
                }else{
                    $alert='<p class=msg_error>Error al crear el Proveedor.</p>';
                }
            }
    }


?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php" ?>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Registro Proveedor</title>
</head>
<body>
	<?php include "includes/header.php" ?>
	<section id="container">
        <div class="form_register">
            <h1><i class="fas fa-user-plus"></i>  Registro Proveedor</h1>
            <hr>
            <div class="alert"><?php echo isset($alert) ? $alert : '';?></div>

            <form action="" method="post">
                <label for="tipo_documento">Tipo Documento</label>
                <select name="tipo_documento" id="tipo_documento">
                    <option value="Cédula Ciudadanía">Cédula Ciudadanía</option>
                    <option value="Cédula Extranjería">Cédula Extranjería</option>
                    <option value="Pasaporte">Pasaporte</option>
                    <option value="NIT">NIT</option>
                    <option value="Permiso Especial">Permiso Especial</option>
                </select>
                <label for="idcliente">N° Documento *</label>
                <input type="text" name="idproveedor" id="idproveedor" placeholder="N° Documento" required>
                <label for="nombre">Nombre *</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre completo" required>
                <label for="direccion">Dirección *</label>
                <input type="text" name="direccion" id="direccion" placeholder="Dirección" required>
                <label for="telefono">Teléfono *</label>
                <input type="text" name="telefono" id="telefono" placeholder="Teléfono" required>
                <label for="correo">Correo Electrónico</label>
                <input type="email" name="correo" id="correo" placeholder="Correo electrónico">
                <label for="empresa">Empresa</label>
                <input type="text" name="empresa" id="empresa" placeholder = "Empresa">
                <label for="instagram">Instagram</label>
                <input type="text" name="instagram" id="instagram" placeholder="Instagram">
                <label for="pagina_web">Página Web</label>
                <input type="text" name="pagina_web" id="pagina_web" placeholder="Página Web">
                <button type="submit" class="btn_save"><i class="far fa-save"></i> Registrar Proveedor</button>
            </form>        
        </div>

	</section>

	<?php include "includes/footer.php" ?>
</body>
</html>