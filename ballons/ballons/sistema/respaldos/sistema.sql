SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
--
-- Database: `sistema`
--




CREATE TABLE `pedidos` (
  `idpedido` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `documento` int(11) NOT NULL,
  `fecha_nacimiento` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `hora` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `direccion` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `total_pedido` float NOT NULL,
  `domicilio` float NOT NULL,
  `monto_total` float NOT NULL,
  `abono` float NOT NULL,
  `saldo` float NOT NULL,
  `forma_pago` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `domiciliario` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `estatus` int(11) NOT NULL,
  `entregado` varchar(25) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'Por Entregar',
  PRIMARY KEY (`idpedido`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


INSERT INTO pedidos VALUES
("81","Julisa Beatriz","Mejia Azuaje","1492161","1977-01-30","jucila@hotmail.com","2022-11-10","5:55 pm","local","un bouquet","5000","10000","15000","12000","3000","Transferencia","Jose","1","Entregado"),
("82","Sebastian Enrique","Sosa Araujo","1510351","2011-12-14","faraujorivas@hotmail.com","2022-11-12","18:46","local","fdggfg","29000","10000","39000","20000","19000","Efectivo","","0","Por Entregar"),
("83","Fabiola Karina","Araujo Rivas","1588956","1973-11-01","faraujorivas@hotmail.com","2022-11-10","15:00","local","un arreglo","29000","10000","39000","30000","9000","Efectivo","","1","Entregado"),
("84","Candice","Araujo","1588956","2000-12-12","jucila@hotmail.com","2022-11-10","14:00","local","prueba de pedido","19000","10000","29000","20000","9000","Efectivo","jose","1","Entregado"),
("85","Fabiola Karina","Araujo Rivas","1492161","","faraujorivas@hotmail.com","2023-04-20","2:00 pm","retiro en tienda","corsage hbd","35000","0","35000","20000","15000","Transferencia","","1","Entregado");




CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `precio` float NOT NULL,
  `existencia` int(11) NOT NULL,
  `minimo` int(11) NOT NULL,
  `margen_ganancia` int(11) NOT NULL,
  `imagen` blob NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_producto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


INSERT INTO productos VALUES
("1234","GLOBO","GLOBO","1000","100","12","10","","1","1"),
("34567","BANDERIN","BANDERIN","100000","12","1","12","img_producto.png","1","1");




CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `clave` int(11) NOT NULL,
  `rol` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


INSERT INTO usuario VALUES
("1","Fabiola Araujo","faraujorivas@hotmail.com","Administrador","1234","3","1");




/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;