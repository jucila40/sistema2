<?php
session_start();
    if($_SESSION['rol'] != 1 and $_SESSION['rol'] != 2)
    {
        header("location: ./");
    }

    include "../conexion.php";

    if(!empty($_POST))
    {
        
        $idcliente = $_POST['idcliente'];

        $query_delete = mysqli_query($conn,"UPDATE clientes SET estatus = 0 WHERE id_cliente = $idcliente");
        mysqli_close($conn);
        if($query_delete)
        {
            header("Location: lista_cliente.php");
            mysqli_close($conn);
        }else{
            echo "Error al eliminar";
        }

    }

    if(empty($_REQUEST['id']))
    {
        header("Location: lista_cliente.php");
    }else{
      

        $idcliente = $_REQUEST['id'];

        $query = mysqli_query($conn,"SELECT * FROM clientes
                                        WHERE id_cliente = $idcliente");
        mysqli_close($conn);
        $result = mysqli_num_rows($query);

        if($result > 0)
        {
            while ($data = mysqli_fetch_array($query)){
                $id = $data['id_cliente'];
                $nombre = $data['nombre_cliente'];
                $telefono = $data['telefono_cliente'];
                
            }
        }else{
            header("Location: lista_cliente.php");
        }
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<?php include "includes/scripts.php" ?>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/functions.js"></script>
	<title>Eliminar Cliente</title>
</head>
<body>
	<?php include "includes/header.php" ?>
	<section id="container">
		<div class="data_delete">
            <i class="fas fa-user-times fa-7x" style = "color: #cc2a2a"></i>
            <br><br><br>
            <h2>¿Está seguro de eliminar el siguiente registro?</h2>
            <br>
            <p>Documento: <span><?php echo $id; ?></span> </p>
            <p>Nombre: <span><?php echo $nombre; ?></span> </p>
            <p>Telefono: <span><?php echo $telefono; ?></span> </p>
            <br>
            <form action="" method = "post">
                <input type="hidden" name="idcliente" value="<?php echo $idcliente; ?>">
                <a href="lista_cliente.php" class="btn_cancel"><i class="fas fa-ban"></i>  Cancelar</a>
                <button type="submit" class="btn_ok"><i class="fas fa-trash"></i>  Aceptar</button>
            </form>
        </div>
	</section>

	<?php include "includes/footer.php" ?>
</body>
</html>