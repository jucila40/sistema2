<?php
session_start();

include "../conexion.php";

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <?php include "includes/scripts.php" ?>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/functions.js"></script>
    <title>Lista de Clientes</title>
</head>

<body>
    <?php include "includes/header.php" ?>
    <section id="container">
        <?php
        $busqueda = strtolower($_REQUEST['busqueda']);
        if (empty($busqueda)) {
            header("Location: lista_cliente.php");
            mysqli_close($conn);
        }
        ?>

        <h1><i class="far fa-address-card"></i> Lista de Clientes</h1>
        <a href="registro_cliente.php" class="btn_new"><i class="fas fa-user-plus"></i> Crear Cliente</a>
        <form action="buscar_cliente.php" method="get" class="form_search">
            <input type="text" name="busqueda" id=busqueda placeholder="Buscar" value="<?php echo $busqueda; ?>">
            <button type="submit" class="btn_search"> <i class="fas fa-search"> </i></button>
        </form>


        <table>
            <tr>
                <th>Documento</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Correo</th>
                <th>Fecha Nacimiento</th>
                <th>Instagram</th>
                <th>Acciones</th>
            </tr>

            <?php
            //paginador

            $sql_register = mysqli_query($conn, "SELECT COUNT(*) as total_registro FROM clientes 
                                                WHERE (id_cliente LIKE '%$busqueda%' OR
                                                nombre_cliente LIKE '%$busqueda%' OR
                                                email_cliente LIKE '%$busqueda%' OR
                                                instagram_cliente LIKE '%$busqueda%')
                                                AND estatus = 1");
            $result_register = mysqli_fetch_array($sql_register);

            $total_registro = $result_register['total_registro'];
            $por_pagina = 5;

            if (empty($_GET['pagina'])) {
                $pagina = 1;
            } else {
                $pagina = $_GET['pagina'];
            }

            $desde = ($pagina - 1) * $por_pagina;
            $total_paginas = ceil($total_registro / $por_pagina);

            $query = mysqli_query($conn, "SELECT * FROM clientes 
                                          WHERE 
                                          (id_cliente LIKE '%$busqueda%' OR
                                            nombre_cliente LIKE '%$busqueda%' OR
                                            email_cliente LIKE '%$busqueda%' OR
                                            instagram_cliente LIKE '%$busqueda%') AND
                                          estatus = 1 LIMIT $desde,$por_pagina");
            mysqli_close($conn);
            $result = mysqli_num_rows($query);
            if ($result > 0) {
                while ($data = mysqli_fetch_array($query)) {
            ?>

                    <tr>
                        <td><?php echo $data['id_cliente']; ?></td>
                        <td><?php echo $data['nombre_cliente']; ?></td>
                        <td><?php echo $data['direccion_cliente']; ?></td>
                        <td><?php echo $data['telefono_cliente']; ?></td>
                        <td><?php echo $data['email_cliente']; ?></td>
                        <td><?php echo $data['fecha_nacimiento']; ?></td>
                        <td><?php echo $data['instagram_cliente']; ?></td>
                        <td>
                            <a class="link_edit" href="editar_cliente.php?id=<?php echo $data['id_cliente']; ?>"><i class="fas fa-user-edit"></i> Editar</a>
                            <?php
                            if ($_SESSION['rol'] == 1 || $_SESSION['rol'] == 2) {
                            ?>
                                |
                                <a class="link_delete" href="eliminar_confirmar_cliente.php?id=<?php echo $data['id_cliente']; ?>"><i class="fas fa-trash"></i> Eliminar</a>
                            <?php
                            }
                            ?>
                    </tr>

            <?php
                }
            }
            ?>
        </table>
        <?php
        if ($total_registro != 0) {
        ?>
            <div class="paginador">
                <ul>
                    <?php
                    if ($pagina != 1) {
                    ?>
                        <li><a href="?pagina=<?php echo 1; ?>&busqueda=<?php echo $busqueda; ?>"><i class="fas fa-step-backward"></i></a></li>
                        <li><a href="?pagina=<?php echo $pagina - 1; ?>&busqueda=<?php echo $busqueda; ?>"><i class="fas fa-backward"></i></a></li>
                    <?php
                    }
                    ?>
                    <?php
                    for ($i = 1; $i <= $total_paginas; $i++) {
                        if ($i == $pagina) {
                            echo '<li class="pageSelected">' . $i . '</li>';
                        } else {
                            echo '<li><a href="?pagina=' . $i . '&busqueda=' . $busqueda . '">' . $i . '</a></li>';
                        }
                    }
                    ?>
                    <?php
                    if ($pagina != $total_paginas) {
                    ?>
                        <li><a href="?pagina=<?php echo $pagina + 1; ?>&busqueda=<?php echo $busqueda; ?>"><i class="fas fa-forward"></i></a></li>
                        <li><a href="?pagina=<?php echo $total_paginas; ?>"><i class="fas fa-step-forward"></i></a></li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        <?php
        }
        ?>
    </section>

    <?php include "includes/footer.php" ?>
</body>

</html>