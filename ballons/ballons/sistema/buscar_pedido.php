<?php
session_start();

include "../conexion.php";

?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <?php include "includes/scripts.php" ?>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/functions.js"></script>
    <title>Lista de Pedidos</title>
</head>

<body>
    <?php include "includes/header.php" ?>
    <section id="container">
        <?php
        $busqueda = strtolower($_REQUEST['busqueda']);
        if (empty($busqueda)) {
            $busqueda = '';
        }
        ?>
        <h1><i class="fas fa-gifts"></i> Lista de Pedidos</h1>
        <a href="registro_pedido.php" class="btn_new"><i class="fas fa-user-plus"></i> Crear Pedido</a>
        <form action="buscar_pedido.php" method="get" class="form_search">
            <input type="text" name="busqueda" id=busqueda placeholder="Buscar">
            <button type="submit" class="btn_search"> <i class="fas fa-search"> </i></button>
        </form>


        <table>
            <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Dirección</th>
                <th>Fecha Entrega</th>
                <th>Hora</th>
                <th>Estatus</th>
                <th>Acciones</th>
            </tr>

            <?php
            //paginador
            $sql_register = mysqli_query($conn, "SELECT COUNT(*) as total_registro FROM pedidos 
                                                WHERE (nombres LIKE '%$busqueda%' OR apellidos LIKE '%$busqueda%') AND estatus = 1");
            $result_register = mysqli_fetch_array($sql_register);

            $total_registro = $result_register['total_registro'];
            $por_pagina = 10;

            if (empty($_GET['pagina'])) {
                $pagina = 1;
            } else {
                $pagina = $_GET['pagina'];
            }

            $desde = ($pagina - 1) * $por_pagina;
            $total_paginas = ceil($total_registro / $por_pagina);

            $query = mysqli_query($conn, "SELECT * FROM pedidos WHERE (nombres LIKE '%$busqueda%' OR apellidos LIKE '%$busqueda%') AND estatus = 1 AND entregado = 'Por Entregar' ORDER BY fecha,hora ASC LIMIT $desde,$por_pagina ");

            $result = mysqli_num_rows($query);
            if ($result > 0) {
                while ($data = mysqli_fetch_array($query)) {
            ?>

                    <tr>
                        <td><?php echo $data['nombres']; ?></td>
                        <td><?php echo $data['apellidos']; ?></td>
                        <td><?php echo $data['direccion']; ?></td>
                        <td><?php echo date("d/m/Y", strtotime($data['fecha'])); ?></td>
                        <td><?php echo date("g:i a", strtotime($data['hora'])); ?></td>
                        <td><?php echo $data['entregado']; ?></td>
                        </td>
                        <td>

                            <a class="link_add" pedido="<?php echo $data['idpedido']; ?>" href="ver_pedido.php?id=<?php echo $data['idpedido']; ?>"><i class=" fas fa-plus-circle"></i> Visualizar</a>
                            <a class="link_edit" href="editar_pedido.php?id=<?php echo $data['idpedido']; ?>"><i class="fas fa-user-edit"></i> Editar</a>
                            <a class="link_delete del_pedido" href="eliminar_pedido.php?id=<?php echo $data['idpedido']; ?>" pedido="<?php echo $data['idpedido']; ?>"><i class="fas fa-trash"></i> Eliminar</a>

                        </td>
                    </tr>

            <?php
                }
            }
            ?>
        </table>
        <div class="paginador">
            <ul>
                <?php
                if ($pagina != 1) {
                ?>
                    <li><a href="?pagina=<?php echo 1; ?>"><i class="fas fa-step-backward"></i></a></li>
                    <li><a href="?pagina=<?php echo $pagina - 1; ?>"><i class="fas fa-backward"></i></a></li>
                <?php
                }
                ?>
                <?php
                for ($i = 1; $i <= $total_paginas; $i++) {
                    if ($i == $pagina) {
                        echo '<li class="pageSelected">' . $i . '</li>';
                    } else {
                        echo '<li><a href="?pagina=' . $i . '">' . $i . '</a></li>';
                    }
                }
                ?>
                <?php
                if ($pagina != $total_paginas) {
                ?>
                    <li><a href="?pagina=<?php echo $pagina + 1; ?>"><i class="fas fa-forward"></i></a></li>
                    <li><a href="?pagina=<?php echo $total_paginas; ?>"><i class="fas fa-step-forward"></i></a></li>
                <?php
                }
                ?>
            </ul>
        </div>
    </section>

    <?php include "includes/footer.php" ?>
</body>

</html>