<?php
session_start();
if ($_SESSION['rol'] != 1) {
    header("Location: ./");
}
include "../conexion.php";

if (!empty($_POST)) {
    $alert = '';
    if (empty($_POST['nombre']) || empty($_POST['correo']) || empty($_POST['usuario']) || empty($_POST['rol'])) {
        $alert = '<p class=msg_error>Todos los campos son obligatorios.</p>';
    } else {
        $idusuario = $_POST['id'];
        $nombre = $_POST['nombre'];
        $email = $_POST['correo'];
        $user = $_POST['usuario'];
        $clave = $_POST['clave'];
        $rol = $_POST['rol'];


        $query = mysqli_query($conn, "SELECT * FROM usuario 
                                                   WHERE (usuario = '$user' AND idusuario != '$idusuario')
                                                   OR (correo = '$email' AND idusuario != '$idusuario')");
        $result = mysqli_fetch_array($query);
        //result = count($result);
        if ($result > 0) {
            $alert = '<p class=msg_error>El correo o el usuario ya existe.</p>';
        } else {
            if (empty($_POST['clave'])) {
                $sql_update = mysqli_query($conn, "UPDATE usuario SET nombre = '$nombre', correo = '$email', usuario = '$user', rol = '$rol'  
                                                WHERE idusuario = '$idusuario'");
            } else {
                $sql_update = mysqli_query($conn, "UPDATE usuario SET nombre = '$nombre', correo = '$email', usuario = '$user', clave = '$clave', rol = '$rol'  
                                                WHERE idusuario = '$idusuario'");
            }

            if ($sql_update) {
                $alert = '<p class=msg_save>Usuario actualizado correctamente.</p>';
            } else {
                $alert = '<p class=msg_error>Error al actualizar el usuario.</p>';
            }
        }
    }
}


//Mostrar Datos    
if (empty($_REQUEST['id'])) {
    header('Location: lista_usuario.php');
    mysqli_close($conn);
}

$iduser = $_REQUEST['id'];
$sql = mysqli_query($conn, "SELECT u.idusuario, u.nombre, u.correo, u.usuario, (u.rol) as idrol, (r.rol) FROM usuario u INNER JOIN rol r ON u.rol = r.idrol WHERE idusuario = $iduser and status = 1");
$result_sql = mysqli_num_rows($sql);

if ($result_sql == 0) {
    header('Location: lista_usuario.php');
} else {
    $option = '';
    while ($data = mysqli_fetch_array($sql)) {

        $iduser = $data['idusuario'];
        $nombre = $data['nombre'];
        $correo = $data['correo'];
        $usuario = $data['usuario'];
        $idrol = $data['idrol'];
        $rol = $data['rol'];

        if ($idrol == 1) {
            $option = '<option value="' . $idrol . '"select>' . $rol . '</option>';
        } elseif ($idrol == 2) {
            $option = '<option value="' . $idrol . '"select>' . $rol . '</option>';
        } elseif ($idrol == 3) {
            $option = '<option value="' . $idrol . '"select>' . $rol . '</option>';
        }
    }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <?php include "includes/scripts.php" ?>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <script type="text/javascript" src="js/functions.js"></script>
    <title>Actualizar Usuario</title>
</head>

<body>
    <?php include "includes/header.php" ?>
    <section id="container">
        <div class="form_register">
            <h1><i class="fas fa-user-edit"></i> Actualizar Usuario</h1>
            <hr>
            <div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

            <form action="" method="post">
                <input type="hidden" name="id" value=<?php echo $iduser; ?>>
                <label for="nombre">Nombre</label>
                <input type="text" name="nombre" id="nombre" placeholder="Nombre completo" value="<?php echo $nombre; ?>">
                <label for="correo">Correo Electrónico</label>
                <input type="email" name="correo" id="correo" placeholder="Correo electrónico" value="<?php echo $correo; ?>">
                <label for="usuario">Usuario</label>
                <input type="text" name="usuario" id="usuario" placeholder="Usuario" value="<?php echo $usuario; ?>">
                <label for="clave">Clave</label>
                <input type="password" name="clave" id="clave" placeholder="Clave de acceso">
                <label for="rol">Tipo Usuario</label>
                <?php
                include "../conexion.php";
                $query_rol = mysqli_query($conn, "SELECT * FROM rol");

                $result_rol = mysqli_num_rows($query_rol);
                ?>
                <select name="rol" id="rol" class="notItemOne">
                    <?php
                    echo $option;
                    if ($result_rol > 0) {
                        while ($rol = mysqli_fetch_array($query_rol)) {
                    ?>
                            <option value="<?php echo $rol["idrol"]; ?>"><?php echo $rol["rol"]; ?></option>
                    <?php
                        }
                    }
                    ?>

                </select>
                <button type="submit" class="btn_save"><i class="fas fa-edit"></i> Actualizar Usuario</button>
            </form>
        </div>

    </section>

    <?php include "includes/footer.php" ?>
</body>

</html>