<?php
$alert = '';
session_start();
if (!empty($_SESSION['active'])) {
    header('location: sistema/');
} else {


    if (!empty($_POST)) {
        if (empty($_POST['usuario']) || empty($_POST['clave'])) {
            $alert = "Ingrese su usuario y su clave";
        } else {
            require_once "conexion.php";
            $user = mysqli_real_escape_string($conn, $_POST['usuario']);
            $pass = mysqli_real_escape_string($conn, $_POST['clave']);
            echo ("estoy acá" . $user);
            echo ("estoy acá" . $pass);



            $query = mysqli_query($conn, "SELECT * from usuario WHERE usuario = '$user'
                    AND clave = '$pass'");
            mysqli_close($conn);
            $result = mysqli_num_rows($query);

            echo ($result);
            if ($result > 0) {
                $data = mysqli_fetch_array($query);
                $_SESSION['active'] = true;
                $_SESSION['idUser'] = $data['idusuario'];
                $_SESSION['nombre'] = $data['nombre'];
                $_SESSION['email'] = $data['correo'];
                $_SESSION['user'] = $data['usuario'];
                $_SESSION['rol'] = $data['rol'];

                header('location: sistema/');
            } else {
                $alert = "El usuario o la clave son incorrectos";
                session_destroy();
            }
        }
    }
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | Sistema Ballons & Decoration</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
    <section id="container">

        <form action="" method="post">
            <h3>Iniciar Sesión</h3>
            <img src="img/logo.jpg" alt="Login" width="100" height="100">
            <input type="text" name="usuario" placeholder="Usuario">
            <input type="password" name="clave" placeholder="Contraseña">
            <div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>
            <input type="submit" value="INGRESAR">
        </form>
    </section>
</body>

</html>